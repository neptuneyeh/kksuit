<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Helper\KKBOXOpenApiHelper;

class KkboxOpenApiTest extends TestCase
{
    protected $kkboxOpenApiHelper = null;

    protected function setUp(): void
    {
        parent::setUp();
        // make sure is correct access token
        $accessToken = config('global.test_access_token');
        $this->kkboxOpenApiHelper = new KKBOXOpenApiHelper($accessToken);
    }

    /** @test */
    public function testGetProfileInformation()
    {
        $response = $this->kkboxOpenApiHelper->getProfileInformation();
        $this->assertIsObject($response);
    }

    /** @test */
    public function testGetPrivatePlaylists()
    {
        $response = $this->kkboxOpenApiHelper->getPrivatePlaylists();
        $this->assertIsObject($response);
    }

    /** @test */
    public function testGetRecommendedTracksBasedOnRecentPlayHistory()
    {
        $response = $this->kkboxOpenApiHelper->getRecommendedTracksBasedOnRecentPlayHistory();
        $this->assertIsObject($response);
    }

    /** @test */
    public function testGetPublicSharedPlaylists()
    {
        $response = $this->kkboxOpenApiHelper->getPublicSharedPlaylists();
        $this->assertIsObject($response);
    }

    /** @test */
    public function testCreatePrivatePlaylist()
    {
        $response = $this->kkboxOpenApiHelper->createPrivatePlaylist([
            'name' => 'unit-test-play-list'
        ]);
        $this->assertIsObject($response);
    }

    /** @test */
    public function testGetTracksOfAPrivatePlaylist()
    {
        $response = $this->kkboxOpenApiHelper->getTracksOfAPrivatePlaylist('T-wzf6BIqUbIautjRI');
        $this->assertIsObject($response);
    }

    /** @test */
    public function testAddTracksToPrivatePlaylist()
    {
        $response = $this->kkboxOpenApiHelper->addTracksToPrivatePlaylist('T-wzf6BIqUbIautjRX', [
            'track_ids' => [
                'TajP_qpHNCtw8MZX8H'
            ]
        ]);
        $this->assertIsObject($response);
    }
}
