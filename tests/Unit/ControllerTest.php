<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Http\Controllers\Controller;

class ControllerTest extends TestCase
{
    /** @test */
    public function testResJson()
    {
        $controller = new Controller();
        $responeData = json_decode($controller->resJson()->content());

        $this->assertIsObject($responeData);
        $this->assertObjectHasAttribute('locale', $responeData);
        $this->assertObjectHasAttribute('status', $responeData);
        $this->assertObjectHasAttribute('message', $responeData);
        $this->assertObjectHasAttribute('data', $responeData);
        $this->assertIsArray($responeData->data);
    }

}
