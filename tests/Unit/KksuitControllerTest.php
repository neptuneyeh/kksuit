<?php

namespace Tests\Unit;

//use PHPUnit\Framework\TestCase;
use Tests\TestCase;
use Illuminate\Support\Facades\Session;

class KksuitControllerTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();
    }

    /** @test */
    public function a_not_login_user_access_index()
    {
        $response = $this->get('/');

        $response->assertViewIs('kksuit_login');
    }

    /** @test */
    public function a_login_user_access_index()
    {
        // make sure is correct access token
        Session::put('kkbox_api_token', config('global.test_access_token'));
        $response = $this->get('/');

        $response->assertViewIs('kksuit_main');
    }


}
