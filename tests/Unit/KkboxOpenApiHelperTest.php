<?php

namespace Tests\Unit;

use App\Helper\KKBOXOpenApiHelper;
use Tests\TestCase;

class KkboxOpenApiHelperTest extends TestCase
{
    protected $kkboxOpenApiHelper = null;

    protected function setUp(): void
    {
        parent::setUp();
        // make sure is correct access token
        $accessToken = config('global.test_access_token');
        $this->kkboxOpenApiHelper = new KKBOXOpenApiHelper($accessToken);
    }

    /** @test */
    public function testCheckPlaylistIfInvalidAuthentication()
    {
        $this->ccessToken = '8ZLJFybR8IUvE75jz6PoKg==Error';
        $this->kkboxOpenApiHelper = new KKBOXOpenApiHelper($this->ccessToken);
        $result = $this->kkboxOpenApiHelper->checkPlaylist();
        $this->assertFalse($result);
    }

    /** @test */
    public function testCheckPlaylist()
    {
        $result = $this->kkboxOpenApiHelper->checkPlaylist();
        $this->assertTrue($result);
    }

    /** @test */
    public function testGetKksuitListTracks()
    {
        // playlists must have kksuit-like-list & kksuit-dislike-list
        $testPlaylistsObject = (object)[
            'data' => [
                (object)[
                    'id' => 'StacblLaJ9b8GP54sq',
                    'title' => 'kksuit-like-list',
                    'images' => []
                ],
                (object)[
                    'id' => 'StccflLaJ9b8GP55sq',
                    'title' => 'kksuit-dislike-list',
                    'images' => []
                ]

            ]
        ];

        $reflector = new \ReflectionMethod(KKBOXOpenApiHelper::class, 'getKksuitListTracks');
        $reflector->setAccessible(true);
        $result = $reflector->invoke($this->kkboxOpenApiHelper, $testPlaylistsObject);

        $this->assertIsArray($result);
        $this->assertArrayHasKey('kksuitLikeId', $result);
        $this->assertArrayHasKey('kksuitDislikeId', $result);
        $this->assertArrayHasKey('like', $result);
        $this->assertArrayHasKey('dislike', $result);
        $this->assertIsArray($result['like']);
        $this->assertIsArray($result['dislike']);
    }

    /** @test */
    public function testGetRecommendedTracks()
    {
        $testTracksObject = (object)[
            'tracks' => (object)[
                'data' => [
                    (object)[
                        'id' => 'TYaDjto1GziN6efxSx',
                        'name' => 'test_song_1'
                    ],
                    (object)[
                        'id' => 'X_8bbX2exGbE7C1Dem',
                        'name' => 'test_song_2',
                    ]
                ]
            ]
        ];

        $reflector = new \ReflectionMethod(KKBOXOpenApiHelper::class, 'getRecommendedTracks');
        $reflector->setAccessible(true);
        $result = $reflector->invoke($this->kkboxOpenApiHelper, $testTracksObject);

        $this->assertIsArray($result);
    }

    /** @test */
    public function testFormatRecommendedTracks()
    {
        $testTracksArray = [
            "P-uWn4MWhnTeO9K1ej",
            "-tERQLGpZ3RLtraaHq",
            "SmhZxKQp2bwp4c-ehi",
            "5aGJ46DjyKyO4i-BRK"
        ];

        $testKksuitTracks = [
            'kksuitLikeId' => 'StacblLaJ9b8GP54sq',
            'kksuitDislikeId' => 'StccflLaJ9b8GP55sq',
            'like' => [],
            'dislike' => []
        ];

        $reflector = new \ReflectionMethod(KKBOXOpenApiHelper::class, 'formatRecommendedTracks');
        $reflector->setAccessible(true);
        $result = $reflector->invoke($this->kkboxOpenApiHelper, $testTracksArray, $testKksuitTracks);

        $this->assertIsArray($result);
    }
}
