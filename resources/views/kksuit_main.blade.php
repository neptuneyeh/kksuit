<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>KKSuit</title>

        <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js" integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF" crossorigin="anonymous"></script>

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js" ></script>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;600&display=swap" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">

            <div class="top-right links">
                @if (\App\Http\Controllers\Auth\KkboxLoginController::checkLogin())
                    <a class="nav-link" href="{{ url('/logout') }}"> Logout </a>
                @endif
            </div>


            <div class="content">
                <div class="title m-b-md">
                    歡迎使用 KKSuit
                </div>

                @if ( isset($userProfile) )
                    <div class="card" style="width: 18rem;">
                        <img class="card-img-top" src="{{ $userProfile->images[2]->url }}" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">{{ $userProfile->name }}</h5>
                        </div>
                    </div>

                    <div>
                        <h3>您看起來有些不錯的歌單!</h3>
                        <ul class="list-group">
                            @foreach ($privatePlaylists->data as $item)

                                <li class="list-group-item">{{$item->title}}</li>
                            @endforeach
                        </ul>
                    </div>

                    <div>
                        <h3>我們依據您的喜好想問問看你喜不喜歡以下的歌曲</h3>
                        <iframe id="player" src="" allow="autoplay" ></iframe>
                        <button id="start_listen" type="button" class="btn btn-success">聽起來！</button>
                        <button id="like_this_song" type="button" class="btn btn-success">喜歡</button>
                        <button id="dislike_this_song" type="button" class="btn btn-danger">不喜歡</button>
                    </div>
                @endif

            </div>
        </div>
    </body>

    <script>
        toastr.options = {
            // 參數設定[註1]
            "closeButton": false, // 顯示關閉按鈕
            "debug": false, // 除錯
            "newestOnTop": false,  // 最新一筆顯示在最上面
            "progressBar": true, // 顯示隱藏時間進度條
            "positionClass": "toast-bottom-left", // 位置的類別
            "preventDuplicates": false, // 隱藏重覆訊息
            "onclick": null, // 當點選提示訊息時，則執行此函式
            "showDuration": "300", // 顯示時間(單位: 毫秒)
            "hideDuration": "1000", // 隱藏時間(單位: 毫秒)
            "timeOut": "5000", // 當超過此設定時間時，則隱藏提示訊息(單位: 毫秒)
            "extendedTimeOut": "1000", // 當使用者觸碰到提示訊息時，離開後超過此設定時間則隱藏提示訊息(單位: 毫秒)
            "showEasing": "swing", // 顯示動畫時間曲線
            "hideEasing": "linear", // 隱藏動畫時間曲線
            "showMethod": "fadeIn", // 顯示動畫效果
            "hideMethod": "fadeOut" // 隱藏動畫效果
        }

        @if ( isset($userProfile) )
            $( document ).ready(function() {

                let recommendedCount = 0;
                console.log(recommendedCount);
                let widgetUrlFront = 'https://widget.kkbox.com/v1/?id=';
                let widgetUrlEnd = '&type=song&terr=TW&lang=TC&autoplay=true';
                let kksuitTracks = @json($kksuitTracks);
                let recommendedTracks = @json($recommendedTracks);

                console.log(kksuitTracks);
                console.log(recommendedTracks);

                $("button#start_listen").click(function() {
                    // change src can reload
                    $('iframe#player').attr('src', widgetUrlFront + recommendedTracks[recommendedCount] + widgetUrlEnd);
                });

                $("button#like_this_song").click(function() {
                    // ajax
                    let formData = new FormData();
                    formData.append('play_list_id', kksuitTracks.kksuitLikeId);
                    formData.append('track_id', recommendedTracks[recommendedCount]);
                    formData.append('_token', "{{ csrf_token() }}");

                    let item = {
                        'url': "like-song",
                        'type': 'POST',
                        'async': false,//加入這行代表關閉非同步
                        'contentType': false, //required
                        'dataType': 'json',
                        'processData': false, // required
                        'mimeType': 'multipart/form-data',
                        'data': formData
                    }

                    $.ajax(item)
                        .done(function (response) {
                            toastr.success( "感謝您的喜歡" );
                            // change src can reload
                            recommendedCount++;
                            $('iframe#player').attr('src', widgetUrlFront + recommendedTracks[recommendedCount] + widgetUrlEnd);
                            console.log(response)
                        })

                        .fail(function (response) {
                            console.log(response)
                        })

                });

                $("button#dislike_this_song").click(function() {
                    // ajax
                    let formData = new FormData();
                    formData.append('play_list_id', kksuitTracks.kksuitDislikeId);
                    formData.append('track_id', recommendedTracks[recommendedCount]);
                    formData.append('_token', "{{ csrf_token() }}");

                    let item = {
                        'url': "dislike-song",
                        'type': 'POST',
                        'async': false,//加入這行代表關閉非同步
                        'contentType': false, //required
                        'dataType': 'json',
                        'processData': false, // required
                        'mimeType': 'multipart/form-data',
                        'data': formData
                    }

                    $.ajax(item)
                        .done(function (response) {
                            toastr.warning( "請再聽聽其他推薦><" );
                            // change src can reload
                            recommendedCount++;
                            $('iframe#player').attr('src', widgetUrlFront + recommendedTracks[recommendedCount] + widgetUrlEnd);
                            console.log(response)
                        })

                        .fail(function (response) {
                            console.log(response)
                        })
                });
            });
        @endif
    </script>
</html>
