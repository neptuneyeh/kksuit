# AlexYeh KKBOX Open Api 練習專案 KKSuit

## 使用者痛點描述(假說)
* 雖然目前已經會分析聽過的音樂然後產生推薦歌單，但我還是不會想主動點進去，而且歌也很多
* 推薦的內容我不喜歡的話，可能不喜歡的內容也會一直重複被推薦出來


## 功能描述
一個有串接KKBOX的微服務，使用者可以登入自己的KKBOX帳號，
本功能主動利用使用者曾經聽過的歌曲做推薦歌單，
使用此服務會幫使用者新建兩個歌單(kksuit-like-list&kksuit-dislike-list)。
如果使用者有保留這兩個private歌單，未來再次使用本服務，不喜歡的歌和已經喜歡過的歌就不會再推薦出來

## 預計會使用到的 Api
1. Authorization Code Flow 登入流程
2. Get the private playlists
3. Get a private playlist
4. Create a private playlist
5. Add tracks to private playlist
6. Get recommended tracks based on recent play history
7. KKBOX HTML Widgets

## 安裝
1. ```git clone```
2. ```composer install```
3. .env額外加入
```
CLIENT_ID=your_id
CLIENT_SECRET=you_secret
AUTH_CALLBACK_URL=http://your_domain/callback
不跑test以下這一行可不加
TEST_ACCESSTOKEN=(此toekn必須透過KKBOX OAuth 2.0 API取得)
```

## 附註
1. 不需要用到資料庫
