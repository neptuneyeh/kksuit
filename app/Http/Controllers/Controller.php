<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use App;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $status = 'Fail';
    protected $message = '';
    protected $code = 422;
    protected $data = [];

    public function __construct()
    {

    }

    public function resJson($status = 'success', $message = '', $data = [], $code = 500)
    {
        $ar = [
            'locale' => App::getLocale(),
            'status' => $status,
            'message' => $message,
            'data' => $data
        ];
        return response($ar, $code);
    }
}
