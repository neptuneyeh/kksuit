<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;


class KkboxLoginController extends Controller
{
    protected $clientID = null;
    protected $clientSecret = null;

    function __construct()
    {
        $this->clientID = config('global.client_id');
        $this->clientSecret = config('global.client_id');
    }

    public function login()
    {
        $url = config('global.auth_url');
        $url .= '?redirect_uri=' . urlencode(config('global.redirect_url'));
        $url .= '&client_id=' . $this->clientID;
        $url .= '&response_type=' . config('global.response_type');
        $url .= '&state=true';

        return redirect($url);
    }

    public function authCallBack(Request $request)
    {
        $code = $request->code;
        $state = $request->state;

        $params = [
            'grant_type' => config('global.grant_type'),
            'code' => $code,
            'client_id' => config('global.client_id'),
            'client_secret' => config('global.client_secret')
        ];

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, config('global.access_token_url'));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $params);

        $response = curl_exec($curl);
        curl_close($curl);

        $responseJson = json_decode($response);

        if (property_exists($responseJson, 'access_token')) {
            Session::put('kkbox_api_token', $responseJson->access_token);
        }

        return redirect('/');
    }

    public function logout()
    {
        Session::forget('kkbox_api_token');

        return redirect('/');
    }

    public static function checkLogin(): bool
    {
        return Session::has('kkbox_api_token');
    }
}
