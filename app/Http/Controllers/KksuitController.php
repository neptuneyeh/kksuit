<?php

namespace App\Http\Controllers;

use App\Helper\KKBOXOpenApi;
use App\Helper\KKBOXOpenApiHelper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;


class KksuitController extends Controller
{
    protected $viewData = [];

    /* Public Area */
    public function index()
    {
        if (Session::has("kkbox_api_token")) {

            $kkboxOpenApiHelpr = new KKBOXOpenApiHelper(Session::get("kkbox_api_token"));

            if ($kkboxOpenApiHelpr->checkPlaylist()) {
                $this->viewData = $kkboxOpenApiHelpr->initViewData();
            }

            return view('kksuit_main', $this->viewData);
        }

        return view('kksuit_login');
    }

    public function likeSong(Request $request)
    {
        if (Session::has("kkbox_api_token")) {

            $kkboxOpenApi = new KKBOXOpenApi(Session::get("kkbox_api_token"));

            $playlistId = $request->play_list_id;
            $trackId = $request->track_id;

            $result = $kkboxOpenApi->addTracksToPrivatePlaylist($playlistId, [
                'track_ids' => [
                    $trackId
                ]
            ]);

            if ($result->message == 'Ok') {
                $this->status = "Success";
                $this->code = 200;
            }

            return $this->resJson($this->status, 'success', $result, $this->code);
        }

        return $this->resJson($this->status, 'not login', [], $this->code);
    }

    public function dislikeSong(Request $request)
    {
        if (Session::has("kkbox_api_token")) {

            $kkboxOpenApi = new KKBOXOpenApi(Session::get("kkbox_api_token"));

            $playlistId = $request->play_list_id;
            $trackId = $request->track_id;

            $result = $kkboxOpenApi->addTracksToPrivatePlaylist($playlistId, [
                'track_ids' => [
                    $trackId
                ]
            ]);

            if ($result->message == 'Ok') {
                $this->status = "Success";
                $this->code = 200;
            }

            return $this->resJson($this->status, 'success', $result, $this->code);
        }

        return $this->resJson($this->status, 'not login', [], $this->code);
    }

}
