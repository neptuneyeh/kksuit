<?php

namespace App\Helper;

class KKBOXOpenApi
{
    protected $accessToken;

    function __construct($token)
    {
        $this->accessToken = $token;
    }

    private function post($url, $params)
    {
        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => config('global.kkbox_openapi_url') . $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($params),
            CURLOPT_HTTPHEADER => [
                "accept: application/json",
                "authorization: Bearer " . $this->accessToken
            ],
        ]);

        $response = curl_exec($curl);
        $statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE); //get status code
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return (object) [
                'status_code' => $statusCode,
                'message' => "cURL Error #:" . $err
            ];
        }
        return json_decode($response);
    }

    private function fetch($url)
    {
        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => config('global.kkbox_openapi_url') . $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => [
                "accept: application/json",
                "authorization: Bearer " . $this->accessToken
            ],
        ]);

        $response = curl_exec($curl);
        $statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE); //get status code
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return (object) [
                'status_code' => $statusCode,
                'message' => "cURL Error #:" . $err
            ];
        }
        return json_decode($response);

    }

    public function getProfileInformation()
    {
        return $this->fetch('me');
    }

    public function getPrivatePlaylists()
    {
        return $this->fetch('me/playlists');
    }

    public function getRecommendedTracksBasedOnRecentPlayHistory()
    {
        return $this->fetch('me/recommendations-from-listened?territory=TW');
    }

    public function getPublicSharedPlaylists()
    {
        return $this->fetch('me/shared-playlists');
    }

    public function createPrivatePlaylist($params)
    {
        return $this->post('me/playlists?territory=TW', $params);
    }

    public function getTracksOfAPrivatePlaylist($playlistId)
    {
        return $this->fetch('me/playlists/' . $playlistId . '/tracks?territory=TW');
    }

    public function addTracksToPrivatePlaylist($playlistId, $params)
    {
        return $this->post('me/playlists/' . $playlistId . '/tracks?territory=TW', $params);
    }
}
