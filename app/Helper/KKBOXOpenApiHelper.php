<?php

namespace App\Helper;

class KKBOXOpenApiHelper extends KKBOXOpenApi
{
    function __construct($token)
    {
        parent::__construct($token);
    }

    public function checkPlaylist()
    {
        $kksuitLikeListCreateResponse = null;
        $kksuitDislikeListCreateResponse = null;

        $likeListCreated = false;
        $dislikeListCreated = false;

        // 兩種情況
        // 1.沒有任何list
        // 2.沒有本服務建立的播放清單
        // 3.使用過本服務，但播放清單不完整(缺其中一個)
        // 4.使用過本服務，兩個播放清單都存在
        $privatePlaylists = parent::getPrivatePlaylists();

        if (property_exists($privatePlaylists, 'error')) {
            return false;
        }

        if (count($privatePlaylists->data) == 0) {
            // create kksuit-like-list & kksuit-dislike-list
            $kksuitLikeListCreateResponse = parent::createPrivatePlaylist([
                'name' => 'kksuit-like-list'
            ]);

            $kksuitDislikeListCreateResponse = parent::createPrivatePlaylist([
                'name' => 'kksuit-dislike-list'
            ]);
        } else {
            $playlistTitles = [];
            foreach ($privatePlaylists->data as $item) {
                $playlistTitles[] = $item->title;
            }

            if (!in_array('kksuit-like-list', $playlistTitles)) {
                $kksuitLikeListCreateResponse = parent::createPrivatePlaylist([
                    'name' => 'kksuit-like-list'
                ]);
            }

            if (!in_array('kksuit-dislike-list', $playlistTitles)) {
                $kksuitDislikeListCreateResponse = parent::createPrivatePlaylist([
                    'name' => 'kksuit-dislike-list'
                ]);
            }
        }

        if ($kksuitLikeListCreateResponse == null) {
            $likeListCreated = true;
        } else {
            if (property_exists($kksuitLikeListCreateResponse, 'playlist_id')) {
                $likeListCreated = true;
            }
        }

        if ($kksuitDislikeListCreateResponse == null) {
            $dislikeListCreated = true;
        } else {
            if (property_exists($kksuitDislikeListCreateResponse, 'playlist_id')) {
                $dislikeListCreated = true;
            }
        }

        if ($likeListCreated && $dislikeListCreated) {
            return true;
        }
        return false;
    }

    public function initViewData(): array
    {
        $viewData = [];
        $viewData['userProfile'] = parent::getProfileInformation();
        $viewData['sharedPlaylists'] = parent::getPublicSharedPlaylists();

        $viewData['privatePlaylists'] = parent::getPrivatePlaylists();
        $viewData['kksuitTracks'] = $this->getKksuitListTracks($viewData['privatePlaylists']);

        $recommendedTracks = parent::getRecommendedTracksBasedOnRecentPlayHistory();
        $recommendedTracks = $this->getRecommendedTracks($recommendedTracks);

        // 和喜歡和不喜歡播放清單做比較，不重複推薦
        $viewData['recommendedTracks'] = $this->formatRecommendedTracks($recommendedTracks, $viewData['kksuitTracks']);

        return $viewData;
    }

    protected function getKksuitListTracks($privatePlaylists): array
    {
        $trackArray = [];
        $trackArray['kksuitLikeId'] = null;
        $trackArray['kksuitDislikeId'] = null;
        $trackArray['like'] = [];
        $trackArray['dislike'] = [];

        $kksuitLikeId = null;
        $kksuitDislikeId = null;

        if (count($privatePlaylists->data) > 0) {
            foreach ($privatePlaylists->data as $item) {
                if (property_exists($item, 'title') && property_exists($item, 'id')) {
                    if ($item->title == 'kksuit-like-list') {
                        $kksuitLikeId = $item->id;
                    } elseif ($item->title == 'kksuit-dislike-list') {
                        $kksuitDislikeId = $item->id;
                    }
                }
            }
        }

        $trackArray['kksuitLikeId'] = $kksuitLikeId;
        $trackArray['kksuitDislikeId'] = $kksuitDislikeId;
        $kksuitLikeTracks = parent::getTracksOfAPrivatePlaylist($kksuitLikeId);
        $kksuitDislikeTracks = parent::getTracksOfAPrivatePlaylist($kksuitDislikeId);

        if (is_object($kksuitLikeTracks) && !property_exists($kksuitLikeTracks, 'error')) {
            if (property_exists($kksuitLikeTracks, 'data')) {
                if (count($kksuitLikeTracks->data) > 0) {
                    foreach ($kksuitLikeTracks->data as $item) {
                        if (property_exists($item, 'id')) {
                            $trackArray['like'][] = $item->id;
                        }
                    }
                }
            }
        }

        if (is_object($kksuitDislikeTracks) && !property_exists($kksuitDislikeTracks, 'error')) {
            if (property_exists($kksuitDislikeTracks, 'data')) {
                if (count($kksuitDislikeTracks->data) > 0) {
                    foreach ($kksuitDislikeTracks->data as $item) {
                        if (property_exists($item, 'id')) {
                            $trackArray['dislike'][] = $item->id;
                        }
                    }
                }
            }
        }

        return $trackArray;
    }

    protected function getRecommendedTracks($recommendedTracks): array
    {
        $trackArray = [];
        if (is_object($recommendedTracks) && !property_exists($recommendedTracks, 'error')) {
            if (property_exists($recommendedTracks, 'tracks')) {
                if (count($recommendedTracks->tracks->data) > 0) {
                    foreach ($recommendedTracks->tracks->data as $item) {
                        if (property_exists($item, 'id')) {
                            $trackArray[] = $item->id;
                        }
                    }
                }
            }
        }

        return $trackArray;
    }

    protected function formatRecommendedTracks($recommendedTracks, $kksuitTracks): array
    {
        $trackArray = [];
        $flipped = array_flip($recommendedTracks);

        if (count($kksuitTracks['like']) > 0) {
            foreach ($kksuitTracks['like'] as $item) {
                unset($flipped[$item]);
            }
        }

        if (count($kksuitTracks['dislike']) > 0) {
            foreach ($kksuitTracks['dislike'] as $item) {
                unset($flipped[$item]);
            }
        }

        $temp = array_flip($flipped);
        foreach ($temp as $item) {
            $trackArray[] = $item;
        }

        return $trackArray;
    }
}
