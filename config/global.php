<?php

return [
    'test_access_token' => env('TEST_ACCESSTOKEN', 'YOUR_ACCESS_TOKEN_BY_AuthorizationCodeFlow'),
    'client_id' => env('CLIENT_ID', 'YOUR_CLIENT_ID_BY_KKBOXDEVAPP'),
    'client_secret' => env('CLIENT_SECRET', 'YOUR_CLIENT_SECRET_BY_KKBOXDEVAPP'),
    'auth_url' => 'https://account.kkbox.com/oauth2/authorize',
    'response_type' => 'code',
    'redirect_url' => env('AUTH_CALLBACK_URL', 'https://yourdomain.com/callback'),
    'access_token_url' => 'https://account.kkbox.com/oauth2/token',
    'grant_type' => 'authorization_code',
    'bearer_token_header' => 'Authorization: Bearer ',
    'kkbox_openapi_url' => 'https://api.kkbox.com/v1.1/'
];
