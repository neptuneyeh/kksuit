<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\Auth\KkboxLoginController;
use App\Http\Controllers\KksuitController;

Route::get('/welcome', function () {
    return view('welcome');
});

Route::get('/', [KksuitController::class, 'index']);
Route::get('login', [KkboxLoginController::class, 'login']);
Route::get('logout', [KkboxLoginController::class, 'logout']);
Route::get('auth_callback', [KkboxLoginController::class, 'authCallBack']);
Route::post('like-song', [KksuitController::class, 'likeSong']);
Route::post('dislike-song', [KksuitController::class, 'dislikeSong']);
